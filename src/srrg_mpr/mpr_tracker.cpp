#include "mpr_tracker.h"
#include "srrg_system_utils/system_utils.h"

namespace srrg_mpr {
using srrg_core::getTime;
using namespace std;

void MPRTracker::setImages(const srrg_core::RawDepthImage& raw_depth,
                           const srrg_core::RGBImage& raw_rgb) {
  MPRAligner::setMovingImages(raw_depth, raw_rgb);
}

void MPRTracker::setup() {
  if (!_config_ptr) _config_ptr = new Config;
  MPRAligner::_config_ptr = _config_ptr;
}

MPRTracker::Config& MPRTracker::mutableConfig() {
  cerr << "Tracker::using non const accessor" << endl;
  if (_config_ptr == nullptr) {
    throw std::runtime_error("config invalid. Call setup() after construction");
  }
  _is_ready = false;
  return *_config_ptr;
}

const MPRTracker::Config& MPRTracker::constConfig() const {
  if (_config_ptr == nullptr) {
    throw std::runtime_error("config invalid. Call setup() after construction");
  }
  return *_config_ptr;
}

void MPRTracker::compute() {
  using namespace std;
  init();

  double time_pyramid = getTime();

  // compute the pyramid of the most recent_image , that will be the moving
  buildPyramid(_depth_moving_raw, _rgb_moving_raw);
  _pyramid_generator.compute();
  time_pyramid = getTime() - time_pyramid;
  _stats.time_pyramid = time_pyramid;

  bool switch_keyframe = true;
  if (!_keyframe.empty()) {
    _T = _T * _relative_motion_guess;
    align(_keyframe, _pyramid_generator.pyramid());
    _last_good_frame_in_keyframe_T = _T;
    _global_T = _keyframe_T * _last_good_frame_in_keyframe_T;
    _relative_motion_guess.setIdentity();
    switch_keyframe = false;
  }

  _stats.global_T = _global_T;
  _last_good_frame = _pyramid_generator.pyramid();

  switch_keyframe =
      switch_keyframe ||
      _T.translation().norm() >= constConfig().keyframe_max_translation ||
      fabs(Eigen::AngleAxisf(_T.linear()).angle()) >=
          constConfig().keyframe_max_rotation;

  // keyframe switching
  if (switch_keyframe) {
    _keyframe = _last_good_frame;
    _keyframe_T = _global_T;
    _T.setIdentity();
    _last_good_frame_in_keyframe_T.setIdentity();
  }
  _stats.is_keyframe = switch_keyframe;

  if (constConfig().verbose) {
    cerr << "Alignment" << endl;
    cerr << "Keyframe switch: " << switch_keyframe << endl;
    cerr << "Global T: " << srrg_core::t2v(_global_T).transpose() << endl;
    cerr << "Local T: "
         << srrg_core::t2v(_last_good_frame_in_keyframe_T).transpose() << endl;
  }
}
}
