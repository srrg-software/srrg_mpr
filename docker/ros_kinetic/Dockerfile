# build on top on any image in DockerHub
FROM ros:kinetic-perception

MAINTAINER Igor Bogoslavskyi <igor.bogoslavskyi@uni-bonn.de>

# setup environment
RUN apt-get clean && apt-get update && apt-get install -y locales
RUN locale-gen en_US.UTF-8
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8

CMD ["bash"]

# install essentials
RUN apt-get update && apt-get install --no-install-recommends -yqq \
    openssh-client git build-essential cmake libgtest-dev curl rsync

# install needed software
RUN apt-get update && apt-get install --no-install-recommends -yqq \
    qtbase5-dev libqt5opengl5-dev libqglviewer-dev freeglut3-dev libyaml-cpp-dev

# install catkin-tools
RUN apt-get update && apt-get install -yqq python-pip python-empy \
    && pip install --upgrade pip \
    && pip install --upgrade setuptools \
    && pip install --upgrade catkin_tools \
    && pip install --upgrade catkin_tools_fetch

RUN rm -rf /var/lib/apt/lists/*

RUN git config --global push.default simple
